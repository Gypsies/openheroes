package Core.QueueHandlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

import Connections.Connections;
import Core.Server.Core.ServerCore;
import logging.ServerLogger;



public class WriteHandler implements Runnable {
	private Connections con;
	private SocketChannel cn;
	private CountDownLatch cdl;
	private ServerLogger logging = ServerLogger.getInstance();
	
	public WriteHandler(SocketChannel sc, Connections cn, CountDownLatch count) {
		this.cn = sc;
		this.con = cn;
		this.cdl = count;
	}
	
	public void run() {
			SocketChannel current = this.cn;
			boolean wroteAll = true;
			SelectionKey cKey = null;
			if(this.con.isChannelRegistered(current)) {
			LinkedBlockingQueue<ByteBuffer> writeBuffer = this.con.getConnection(current).getWriteBuffer();
			cKey = this.con.getKeyByChannel(current);
			
			if(writeBuffer != null) {
				while(!writeBuffer.isEmpty()) {
						ByteBuffer tmp = writeBuffer.peek(); //retrieve, but do not remove 1st element
						//Block while writing. Explicitly make sure entire buffer is written.
						int wCount = 0;
						while(tmp.remaining() > 0 && cKey.isWritable()) { 
							try {
								wCount = current.write(tmp);
							} catch (IOException e) {
								this.logging.logMessage(Level.WARNING, this, "Error while writing socket");
								ServerCore.finalizeConnection(this.cn);
							} catch (NullPointerException np) {
								this.logging.logMessage(Level.WARNING, this, "Nullpointer while attempting to write socket. This should not be happening.");
								ServerCore.finalizeConnection(this.cn);
							}
						}
						if(wCount < tmp.limit()) {
							tmp.rewind();
							wroteAll = false;
							break;
						} else {
							writeBuffer.poll(); //retrieve and remove the 1st element if all of its bytes were written
						}
				}
			}
				cKey.interestOps(SelectionKey.OP_READ);
			}
								
				this.cdl.countDown(); //countdown decrement
				
				if(!wroteAll) {
					//wait until channel is ready for writing again
					try {
						while(!cKey.isWritable()) {
							try {
								Thread.sleep(200); //sleep 200ms before trying again	
							} catch (InterruptedException e) {}
						}
						cKey.interestOps(SelectionKey.OP_WRITE);
					} catch (NullPointerException np) {}
				}
	}
}
