package Core.ThreadPools;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;


public class PacketHandlerPool {
	private ExecutorService poolEx; 
	
	public PacketHandlerPool(int nt) {
		this.poolEx = Executors.newFixedThreadPool(nt);
	}
	
	public void executeProcess(Runnable run) {
		this.poolEx.execute(run);
	}
	
	
}
