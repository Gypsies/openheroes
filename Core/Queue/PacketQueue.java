package Core.Queue;

import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;

public class PacketQueue {
	
	private LinkedBlockingQueue<ByteBuffer> readBuffer; 
	private LinkedBlockingQueue<ByteBuffer> writeBuffer;
	
	public PacketQueue(int rsize, int wsize) {
		this.readBuffer = new LinkedBlockingQueue<ByteBuffer>(rsize);
		this.writeBuffer = new LinkedBlockingQueue<ByteBuffer>(wsize);
	}

	public LinkedBlockingQueue<ByteBuffer> getReadBuffer() {
		return readBuffer;
	}

	public LinkedBlockingQueue<ByteBuffer> getWriteBuffer() {
		return writeBuffer;
	}
	

}
